Resttask is simple REST application for special text modification. Default URL of running http://localhost:8080/revert?text=sometext

For example:

   * [http://localhost:8080/revert?text=Ahoj, jak se máš?](http://localhost:8080/revert?text=Ahoj, jak se máš?)
      * returns "?šÁm es kaj ,jOha"


   * [http://localhost:8080/revert?text=Je     mi   fajn.](http://localhost:8080/revert?text=Je     mi   fajn.)
      * returns ".NjaF iM ej"