package blatny.resttask;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class ControllerTest {

	@Autowired
	private MockMvc mockMvc;

	@Test
	public void testNoParams() throws Exception {
		this.mockMvc.perform(get("/revert")).andDo(print()).andExpect(status().is4xxClientError());
	}

	@Test
	public void testOk1() throws Exception {
		this.mockMvc.perform(get("/revert").param("text", "Ahoj, jak se máš?")).andDo(print())
				.andExpect(status().isOk()).andExpect(content().string("?šÁm es kaj ,jOha"));
	}

	@Test
	public void testOk2() throws Exception {
		this.mockMvc.perform(get("/revert").param("text", "Je     mi   fajn.")).andDo(print())
				.andExpect(status().isOk()).andExpect(content().string(".NjaF iM ej"));
	}
}
