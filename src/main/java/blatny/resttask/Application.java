package blatny.resttask;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Application {

	/**
	 * 
	 * URL of running application: http://localhost:8080/revert?text=sometext
	 * For example:
	 * 			http://localhost:8080/revert?text=Ahoj, jak se máš?
	 * 			http://localhost:8080/revert?text=Je     mi   fajn.
	 */
	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}
}
