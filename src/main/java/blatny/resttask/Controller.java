package blatny.resttask;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import blatny.resttask.services.TextService;

@RestController
public class Controller {

	@Autowired
	private TextService textSvc;

	@RequestMapping(path = "/revert", method = RequestMethod.GET)
	public String revert(@RequestParam String text) {
		return textSvc.revertText(text);
	}

}
