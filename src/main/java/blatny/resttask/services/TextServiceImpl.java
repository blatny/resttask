package blatny.resttask.services;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

@Component
public class TextServiceImpl implements TextService {

	private static char[] UPPER_CHARS = new char[] { 'a', 'e', 'i', 'o', 'u' };

	@Override
	public String revertText(final String txt) {
		if (txt == null) {
			return null;
		}
		// merge spaces
		String txtOneSpace = txt.replaceAll("( )+", " ");

		// remove accents
		String txtOneSpaceNoAccent = StringUtils.stripAccents(txtOneSpace);

		// revert text
		StringBuilder sb = new StringBuilder(txtOneSpace.length());
		for (int i = 0; i < txtOneSpace.length(); i++) {
			int revertIndx = txtOneSpace.length() - 1 - i;
			if (revertIndx < 0) {
				break;
			}
			char origChar = txtOneSpaceNoAccent.charAt(i);
			char newChar = txtOneSpace.charAt(revertIndx);
			sb.append(isUpperPosition(origChar) ? Character.toUpperCase(newChar) : Character.toLowerCase(newChar));
		}
		return sb.toString();
	}

	private boolean isUpperPosition(char origChar) {
		for (char uc : UPPER_CHARS) {
			if (uc == origChar) {
				return true;
			}
		}
		return false;
	}

}
